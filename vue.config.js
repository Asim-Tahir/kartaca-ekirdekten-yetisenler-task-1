module.exports = {
	configureWebpack: {
		devServer: {
			clientLogLevel: 'info',
			watchOptions: {
				aggregateTimeout: 300,
				poll: 1000,
				ignored: /node_modules/
			}
		},
	},
	css: {
		loaderOptions: {
			sass: {
				prependData: `
					@import "@/scss/normalize.min.scss";

					@import "./node_modules/bootstrap/scss/_functions.scss";
					@import "./node_modules/bootstrap/scss/_variables.scss";
					@import "./node_modules/bootstrap/scss/_mixins.scss";
					@import "./node_modules/bootstrap/scss/_grid.scss";
					@import "./node_modules/bootstrap/scss/bootstrap-grid.scss";

					/*@import "./node_modules/bootstrap/scss/_reboot.scss";
					@import "./node_modules/bootstrap/scss/_type.scss";
					@import "./node_modules/bootstrap/scss/_images.scss";
					@import "./node_modules/bootstrap/scss/_code.scss";*/

					@import "@/scss/colors.scss";
					@import "@/scss/variables.scss";
					@import url("https://fonts.googleapis.com/css?family=Quicksand:400,500,700&display=swap&subset=latin-ext");`
			}
		}
	}
}