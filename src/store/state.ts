export let state = {
	POST_TITLE_MAX_LENGTH: 50,
	POST_MAX_CONTENT_TEXT_LENGTH: 280,
	TYPES: {
		TEXT_ONLY:'text-only',
		EMBED:'embed',
		IMAGE:'image'
	},
};