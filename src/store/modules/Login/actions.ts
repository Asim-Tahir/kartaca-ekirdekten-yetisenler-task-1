import {createProvider} from '../../../vue-apollo';
import $router from '@/router';
const apolloClient = createProvider();

export const actions = {
	async checkUserLogin (
		{commit}:{commit:any},
		{username, hashedPass}:{username:string,hashedPass:string}
	)
	{
		if(username && hashedPass){
			apolloClient.defaultClient.query({
				query: require('@/graphql/query/userLogin.gql'),
				variables: {
					username: username,
					hashedPass: hashedPass
				}
			}).then((errors) => {
				
				commit('initErrors', errors.data.getErrors);
			});
			//this.errorText = "";
			//this.isErrorShowed = false;
			

			$router.push({
				path: '/Dashboard',
				params: {key: 'KEY'}
			});
		}	
		else{
			//this.errorText = "Kullanıcı adı veya şifre hatalı";
			//this.isErrorShowed = true;
		}
	},
};