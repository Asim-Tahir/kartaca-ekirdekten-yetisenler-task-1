import Login from './Login'

export const modules = {
	Login: {
		namespaced: true,
		state: Login.state,
		getters: Login.getters,
		mutations: Login.mutations,
		actions: Login.actions,
	},
};
