import Vue from 'vue';

export function mapFilters(filters:any) {
	return filters.reduce((result:any, filter: any) => {
			result[filter] = function(...args: any[]) {
					return Vue.prototype.$options.filters[filter](...args);
			};
			return result;
	}, {});
}