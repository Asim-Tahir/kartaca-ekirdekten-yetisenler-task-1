import Vue from 'vue'
import VueMeta from 'vue-meta'
import App from './App.vue'
import router from './router'
import store from './store'
import { createProvider } from './vue-apollo'

Vue.config.productionTip = false;


Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
})

new Vue({
	router,
	store,
	apolloProvider: createProvider(),
	render: h => h(App)
}).$mount('#app')
