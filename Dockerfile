#DEVELOPMENT STAGE
FROM node:lts-alpine as development-stage
WORKDIR /app
COPY package.json ./
ENTRYPOINT [ "yarn" ]
RUN yarn install
COPY . .
EXPOSE 8080:8080
RUN yarn serve

#BUILD STAGE
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package.json ./
RUN npm install --loglevel=error
COPY . .
RUN npm run build

#PRODUCTION STAGE
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html

# Add nginx config
COPY --from=build-stage /app/dist/nginx_prod.conf /tmp/prod.conf
RUN envsubst / < /tmp/prod.conf > /etc/nginx/conf.d/default.conf

COPY /dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]